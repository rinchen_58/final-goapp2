
window.onload = function(){
    fetch("/students")
    .then(res => res.text())
    .then(data => getStudents(data))
    .catch(e =>{
        alert(e)
    }),
    fetch("/courses")
    .then(resp => resp.text())
    .then(data => showCourses(data))
    .catch(e => {
        alert(e)
    }),
    fetch("/showAllEnrolled")
    .then(resp => resp.text())
    .then(data => showAllEnrolled(data))
    .catch(e =>{
        alert(e)
    })
}

function showAllEnrolled(data){
    const enrolls = JSON.parse(data)
    enrolls.forEach(element => {
        newRow(element)
    });
}
function addEnroll() {
    var data = {
        stdid: parseInt(document.getElementById("sid").value),
        courseid: document.getElementById("cid").value
    }
    fetch("/enroll",{
        method:"POST",
        body:JSON.stringify(data),
        headers:{"content-type":"application/json; charset=utf-8"}
    }).then(res => {
        if (res.status == 201) {
            console.log("enrooll")
            fetch("/showEnrolled/"+data.stdid +"/"+data.courseid,)
            .then(resp => resp.text())
            .then(data => shwoDatum(data))
        }else{
            throw new Error(res.statusText)
        }
    }).catch(e =>{
        alert(e)
    })
}


function shwoDatum(data) {
    let info = JSON.parse(data)
    newRow(info)
}

function newRow(info) {

    var table = document.getElementById("myTable")
    var row = table.insertRow(table.length)
    var td =  []

    for (i = 0; i < table.rows[0].cells.length;i++){
        td[i] = row.insertCell(i)
    }
    td[0].innerHTML = info.stdid
    td[1].innerHTML = info.courseid
    td[2].innerHTML = info.date.split("T")[0]
    td[3].innerHTML = '<input type="button" onclick="deleteEnroll(this)" value="Delete" id="button-1"/>'
    resetForm()
}

function resetForm() {
    document.getElementById("sid").value = "";
    document.getElementById("cid").value = "";
}
function deleteEnroll(r) {
    selectedRow = r.parentElement.parentElement
    stdid = selectedRow.cells[0].innerHTML
    courseid = selectedRow.cells[1].innerHTML
    console.log(typeof(id))

    if (confirm(`Are you sure you want to delete the enrollment of student ${stdid} from the course ${courseid}`)){
        fetch("/enroll/"+stdid+"/"+courseid,{
            method:"DELETE",
            headers:{"Content-type":"application/json; charset=UTF-8"}
        }).then(response =>{
            if (response.ok){
                var rowIndex = selectedRow.rowIndex
                if (rowIndex > 0){
                    selectedRow.parentElement.deleteRow(selectedRow.rowIndex)
                }
                selectedRow = null
            }else{
                throw new Error(response.statusText)
            }
        }).catch(e =>{
            alert(e)
        })
    }
}

// function getStudentTable() {
//     console.log("first")
//     fetch("/students")
//     .then(res => res.text())
//     .then(data => showStudent(data))
// }


// function showStudent(data) {
//     console.log("second")
//     students = JSON.parse(data)
//     students.forEach(element => {
//         newRow(element)
//     });
// }

// function newRow(element) {
//     console.log("third")

//     document.getElementById("sid").innerHTML = element.StdID
// }

function getStudents(data) {
    const studentIds = []
   const students = JSON.parse(data)
   console.log(students)
    students.forEach(stud => {
        studentIds.push(stud.StdID)
    });
    console.log(studentIds)
    var select = document.getElementById("sid")
    for (var i = 0; i < studentIds.length; i++){
        var sidd = studentIds[i]
        console.log(sidd)
        var option = document.createElement("option")
        option.innerHTML = sidd
        option.value = sidd
        select.appendChild(option)
    }
}


function showCourses(data) {
    const courseIds = []
    const courses = JSON.parse(data)
    courses.forEach(element => {
        courseIds.push(element.id)
    });
    console.log(courses)
    var select = document.getElementById("cid")
    for (var i = 0; i < courseIds.length; i++){
        var cidd = courseIds[i]
        var option = document.createElement("option")
        option.innerHTML = cidd
        option.value = cidd
        select.appendChild(option)
    }
}