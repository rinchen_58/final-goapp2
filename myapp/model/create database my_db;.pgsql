CREATE DATABASE my_db;

CREATE TABLE student (
    StdID int NOT NULL, 
    FirstName varchar(45) not null,
    MiddleName varchar(45) default null,
    LastName varchar(45) default null,
    Email varchar(45) not null,
    Primary key (StdID),
    Unique(Email)
)

truncate table student