package controller

import (
	"encoding/json"
	"fmt"
	"myapp/model"
	"myapp/utils/date"
	"myapp/utils/httpResponse"
	"net/http"

	"github.com/gorilla/mux"
)


func EnrollStudent(w http.ResponseWriter,r *http.Request) {
	var e model.Enroll
	if err := json.NewDecoder(r.Body).Decode(&e); err != nil{
		httpResponse.ResponseWithError(w,http.StatusBadRequest,"invalid json")
		fmt.Println("invalid json")
		return
	}
	e.Date = date.GetDate()
	fmt.Printf("%T is of thsi type",e.CourseId)
	defer r.Body.Close()
	if enrollErr := e.EnrollStud(); enrollErr != nil {
		httpResponse.ResponseWithError(w,http.StatusBadRequest,"invalid query")
		fmt.Println("cannot enroll",enrollErr)
		return 
	}
	httpResponse.ResponseWithJson(w,http.StatusCreated,map[string]string{"message":"successfully inserted the data"})
	fmt.Println("last")
}

func ShowEnrolled(w http.ResponseWriter, r *http.Request) {
	para := mux.Vars(r)
	stdid := para["stdid"]
	cid := para["cid"]

	stdId, err := getUserID(stdid)
	if err != nil {
		fmt.Println("error in converting the stdid to integer",err)
		return 
	}
	// Cid, cErr := getUserID(cid)
	// if cErr != nil{
	// 	fmt.Println("error in converting the cid to integer",err)
	// 	return
	// }
	var enroll model.Enroll
	if	getErr := enroll.GetEnrolledStudent(stdId,cid); getErr != nil{
		httpResponse.ResponseWithError(w,http.StatusBadRequest,"error in getting the database")
		fmt.Println("kkkk",getErr)
		return 
	}
	httpResponse.ResponseWithJson(w,http.StatusOK,enroll)
}

func GetAllEnrolledDetails(w http.ResponseWriter, r *http.Request) {
	enroll,getErr := model.GetAllEnrolled();
	if getErr != nil{
		httpResponse.ResponseWithError(w,http.StatusNotFound,"data not found")
		return 
	}
	httpResponse.ResponseWithJson(w,http.StatusOK,enroll)
}

func DeleteEnrolled(w http.ResponseWriter,r *http.Request) {
	stdid := mux.Vars(r)["stdid"]
	cid := mux.Vars(r)["courseid"]
	stdId, err := getUserID(stdid)
	if err != nil {
		fmt.Println("error in converting the stdid to integer",err)
		return 
	}
	var enroll model.Enroll
	deErr := enroll.DeleteEnroll(stdId,cid)
	if deErr != nil{
		httpResponse.ResponseWithError(w,http.StatusBadRequest,"couldn't delete the data")
		return 
	}
	httpResponse.ResponseWithJson(w,http.StatusOK,map[string]string{"message":"deleted"})
	fmt.Println("deleted")
}